'''
改良点 
・カメラが垂直でつけられていたら水平に変更しセンサーサイズも変更する
・オーバースキャン時にオフセットの値を利用しているためカメラオフセットできなくなる問題
'''    

import bpy
from bpy.props import (
    IntProperty,
    FloatProperty,
    FloatVectorProperty,
    EnumProperty,
    BoolProperty,
)

bl_info = {
    "name": "overScan",
    "author": "takumi shigyo",
    "version": (1, 0, 0),    
    "blender": (2, 82, 0),
    "support": "COMMUNITY",
    "category": "Camera",
}

class RUN_RESET(bpy.types.Operator):
    bl_label = "reset_overScan"
    bl_idname = "reset.overscan"
    bl_description = ""
    bl_options = {'REGISTER', 'UNDO'}   # undo効くようにする設定
    
    def resetOverScan(self, camObj):
        ''' オーバースキャンで設定したパラメータをリセット
        '''
        bpy.context.scene.render.resolution_x = camObj.data["oriResX"]
        bpy.context.scene.render.resolution_y = camObj.data["oriResY"]
        camObj.data.shift_x = 0
        camObj.data.shift_y = 0
        camObj.data.sensor_width = camObj.data["sensorWidth"]
        
        del camObj.data["oriResX"]
        del camObj.data["oriResY"]
        del camObj.data["sensorWidth"]

    def execute(self, context):
        ''' メイン処理
        '''
        
        camObj = bpy.context.active_object

        if hasattr(camObj.data, "oriResX") == False:    # 初回起動時はこちらでオーバースキャン実行
            self.resetOverScan(camObj)
        elif camObj.data.oriResX > 0:                   # 2回目移行はこちら
            self.resetOverScan(camObj)
        return{'FINISHED'}


class RUN_OVERSCAN(bpy.types.Operator):
    bl_label = "run_overScan"
    bl_idname = "run.overscan"
    bl_description = ""
    bl_options = {'REGISTER', 'UNDO'}   # undo効くようにする設定
    
    def overScan(self, camObj, scaleRatioX, scaleRatioY, pivot):
        ''' オーバースキャン設定
        '''

        # 解像度変更
        curResX = bpy.context.scene.render.resolution_x
        curResY = bpy.context.scene.render.resolution_y
        
        if camObj.data.pixOrPer_prop_enum == 'percent':
            bpy.context.scene.render.resolution_x = curResX * scaleRatioX
            bpy.context.scene.render.resolution_y = curResY * scaleRatioY
        elif camObj.data.pixOrPer_prop_enum == 'pixcel':
            bpy.context.scene.render.resolution_x = camObj.data.magnification_prop_IntX
            bpy.context.scene.render.resolution_y = camObj.data.magnification_prop_IntY

        resResult = bpy.context.scene.render.resolution_y / bpy.context.scene.render.resolution_x

        # センサーフィットが自動だったら水平に変換しセンサーの初期値確定
        curSensor = camObj.data.sensor_width
        if camObj.data.sensor_fit == 'AUTO':
            camObj.data.sensor_fit = 'HORIZONTAL'
            camObj.data.sensor_width = (curResY/curResX*curSensor)/2

        # 元のカメラ設定・解像度をカスタムプロパティに格納
        bpy.types.Camera.oriResX = bpy.props.IntProperty(name="oriResX")
        bpy.types.Camera.oriResY = bpy.props.IntProperty(name="oriResY")
        bpy.types.Camera.sensorWidth = bpy.props.IntProperty(name="sensor_width")
        camObj.data.oriResX = curResX
        camObj.data.oriResY = curResY
        camObj.data.sensorWidth = camObj.data.sensor_width

        # センサーサイズ変更によるオーバースキャン
        sensorWidth = camObj.data.sensor_width
        camObj.data.sensor_width = sensorWidth * scaleRatioX
        
        # オーバースキャン基点設定
        if pivot[0] == 'U':
            LRPivot = 1
        elif pivot[0] == 'D':
            LRPivot = -1
        elif pivot[0] == 'M':
            LRPivot = 0

        if pivot[1] == 'L':
            UDPivot = -1
        elif pivot[1] == 'R':
            UDPivot = 1
        elif pivot[1] == 'M':
            UDPivot = 0
            
        if camObj.data.pixOrPer_prop_enum == 'pixcel':
            scaleRatioX = camObj.data.magnification_prop_IntX / camObj.data.oriResX
            scaleRatioY = camObj.data.magnification_prop_IntY / camObj.data.oriResY

        camObj.data.shift_x = (scaleRatioX-1) / scaleRatioX / 2 * LRPivot
        camObj.data.shift_y = (scaleRatioY-1) / scaleRatioY / 2 * resResult * UDPivot

    def execute(self, context):
        ''' メイン処理
        '''
        pivot = bpy.context.object.data.pivot_prop_enum
        os_x = bpy.context.object.data.magnification_prop_FloatX
        os_y = bpy.context.object.data.magnification_prop_FloatY
        camObj = bpy.context.active_object

        # オーバースキャン実行
        if 'CAMERA' == camObj.type:
            if hasattr(camObj.data, "oriResX") == False:    # 初回起動時はこちらでオーバースキャン実行
                self.overScan(camObj, os_x, os_y, pivot)
            elif camObj.data.oriResX == 0:                  # 2回目移行はこちら
                self.overScan(camObj, os_x, os_y, pivot)
        return{'FINISHED'}
                 
class UI_OVERSCAN(bpy.types.Panel):
    bl_label = "camOverScan"
    bl_idname = "camoverScan.uipanel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"

    @classmethod
    def poll(cls, context):
        ''' 特定のオブジェクトタイプでのみUIを表示
        '''        
        return bpy.context.active_object.type == 'CAMERA'
        
    def draw(self, context):
        ''' UI設定
        '''

        scene = bpy.context.active_object.data

        layout = self.layout

        # オーバースキャンの基点ボタン表示
        row1 = layout.row()
        box1 = row1.box()       

        box_row1 = box1.row()
        box_row1.label(text='pivot:')
        
        box_column1 = box_row1.column(align=True)
        box_row1.grid_flow(columns=3).prop(scene, 'pivot_prop_enum', expand=True)

        
        # オーバースキャン比率か、オーバースキャン後の解像度のいずれかを表示
        box2 = row1.box() 
        box2.alignment = 'CENTER'
        box_row2 = box2.row()
          
        box_row2.prop(scene, "pixOrPer_prop_enum", expand=True)
        
        # 
        box_row3 = box2.row()    
        box_column3 = box_row3.column(align=True)
        
        if scene.pixOrPer_prop_enum == 'percent':
            box_column3.prop(scene, "magnification_prop_FloatX", text="scaleRateX")
            box_column3.prop(scene, "magnification_prop_FloatY", text="scaleRateY")
        elif scene.pixOrPer_prop_enum == 'pixcel':
            box_column3.prop(scene, "magnification_prop_IntX", text="scalePixcelX")
            box_column3.prop(scene, "magnification_prop_IntY", text="scalePixcelY")

        # オーバースキャン実行かリセットの表示
        layout.label(text="overScan:")
        row = layout.row(align=False)
        row.operator("run.overscan", text="overScan", icon='SHADING_BBOX')
        row.operator("reset.overscan", text="reset", icon='LOOP_BACK')


def init_props():
    ''' プロパティの初期化
    '''

    scene = bpy.types.Camera
    #scene = bpy.types.Scene
    
    # オーバースキャン比率プロパティ
    scene.magnification_prop_FloatX = FloatProperty(precision=1, default=1.2, step=10.0)
    scene.magnification_prop_FloatY = FloatProperty(precision=1, default=1.2, step=10.0)
    
    # オーバースキャン後解像度プロパティ
    scene.magnification_prop_IntX = IntProperty(default=2304)
    scene.magnification_prop_IntY = IntProperty(default=1296)

    # オーバースキャン比率か解像度のいずれをえらぶかのプロパティ
    scene.pixOrPer_prop_enum = EnumProperty(
        name='pixOrPer',
        description='select method',
        items=[
            ('percent', 'percent', 'percent'),
            ('pixcel', 'pixcel', 'pixcel'),
        ],
        default='percent'
    )
    
    # オーバースキャンpivot
    scene.pivot_prop_enum = EnumProperty(
        name='pivot',
        description='overscan pivot',
        items=[
            ('UL', '', 'up_left'),
            ('UM', '', 'up_mid'),
            ('UR', '', 'up_right'),
            ('ML', '', 'mid_left'),
            ('MM', '', 'mid_mid'),
            ('MR', '', 'mid_right'),
            ('DL', '', 'down_left'),
            ('DM', '', 'down_mid'),                               
            ('DR', '', 'down_right'),     
        ],
        default='MM'
    )

# 登録クラス名のリスト
classes = (
    RUN_RESET,
    RUN_OVERSCAN,
    UI_OVERSCAN,
)

def register():
    ''' クラス登録
    '''
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
        
    init_props()

def unregister():
    ''' クラス登録解除
    '''
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
    
    scene = bpy.types.Camera
    
    del scene.pixOrPer_prop_enum
    del scene.magnification_prop_FloatX
    del scene.magnification_prop_FloatY
    del scene.magnification_prop_IntX
    del scene.magnification_prop_IntY

if __name__ == "__main__":
    register()